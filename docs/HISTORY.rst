Changelog
=========

2.1.3
-----
 - updated Gitlab-CI

2.1.2
-----
 - updated configuration settings keys

2.1.1
-----
 - updated providers view template

2.1.0
-----
 - added custom OAuth authentication event to be able to handle redirect correctly
   in any client application

2.0.2
-----
 - updated REST API route name and path configuraiton setting name

2.0.1
-----
 - updated modal forms title

2.0.0
-----
 - migrated to Pyramid 2.0

1.3.0
-----
 - added support for MicrosoftOnline OAuth2 provider

1.2.7
-----
 - updated management interface
 - updated providers viewlet
 - small code refactoring to simplify super() calls
 - added missing parent class to OAuth provider connection
 - added support for Python 3.11

1.2.6
-----
 - PyAMS_security interfaces refactoring
 - added support for Python 3.10

1.2.5
-----
 - use new context base add action

1.2.4
-----
 - use constant for principal ID formatter

1.2.3
-----
 - updated ZMI menus context

1.2.2
-----
 - updated add and edit forms title

1.2.1
-----
 - updated add menus registration for last PyAMS_zmi release

1.2.0
-----
 - removed support for Python < 3.7

1.1.4
-----
 - removed Travis-CI configuration

1.1.3
-----
 - updated JSON response in provider add form renderer

1.1.2
-----
 - updated support for new "find_principals" exact match API

1.1.1
-----
 - updated package description for Pypi upload...

1.1.0
-----
 - added OAuth plugin configuration interface
 - added registered OAuth providers to login view
 - updated plugin configuration

1.0.0
-----
 - initial release
